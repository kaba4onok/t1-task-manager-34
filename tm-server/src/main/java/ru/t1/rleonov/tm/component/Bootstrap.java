package ru.t1.rleonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.endpoint.*;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.repository.ISessionRepository;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.repository.IUserRepository;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.endpoint.*;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.repository.ProjectRepository;
import ru.t1.rleonov.tm.repository.SessionRepository;
import ru.t1.rleonov.tm.repository.TaskRepository;
import ru.t1.rleonov.tm.repository.UserRepository;
import ru.t1.rleonov.tm.service.*;
import ru.t1.rleonov.tm.util.SystemUtil;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    {
        registry(domainEndpoint);
        registry(authEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(systemEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User user1 = userService.create("user1", "user1", "user1@mail.ru");
        @NotNull final User user2 = userService.create("user2", "user2", "user2@mail.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(user1.getId(), new Project("FIRST PROJECT", Status.COMPLETED));
        projectService.add(user1.getId(), new Project("A SECOND PROJECT", Status.IN_PROGRESS));
        projectService.add(user2.getId(), new Project("THIRD PROJECT", Status.NOT_STARTED));

        taskService.add(user1.getId(), new Task("FIRST TASK", Status.COMPLETED));
        taskService.add(user1.getId(), new Task("A SECOND TASK", Status.IN_PROGRESS));
        taskService.add(user2.getId(), new Task("THIRD TASK", Status.NOT_STARTED));
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.info("***WELCOME TO TASK-MANAGER***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("***TASK-MANAGER IS SHUTTING DOWN***");
        backup.stop();
    }

}
