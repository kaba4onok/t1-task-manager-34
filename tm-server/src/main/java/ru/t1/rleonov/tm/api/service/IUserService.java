package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password);

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email);

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role);

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    User lockUserByLogin(@Nullable String login);

    User unlockUserByLogin(@Nullable String login);

}
